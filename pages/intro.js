import React from 'react'
import Head from 'next/head'
import TopBar from '../components/TopBar'
import FormStepper from '../components/FormStepper'

const Intro = () => (
  <div>
    <Head>
      <title>Intro</title>
      <link rel="icon" href="/favicon.ico" />
    </Head>

    <TopBar></TopBar>
    <FormStepper></FormStepper>
    </div>
)

export default Intro
