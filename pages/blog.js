import React from 'react'
import Head from 'next/head'
import Nav from '../components/nav'



const Blog = () => {
  
  const getAuthor = () => {
    const authors = [
        "Ben Nelson",
        "Ty Enders",
        "Troy Johnson",
        "Kody Farnsworth",
        "Adam Inglby",
        "Chuck Noris"
    ]

    return authors[Math.floor(Math.random() * authors.length)];
}

const randomDate = (start, end, startHour, endHour) => {
  var date = new Date(+start + Math.random() * (end - start));
  var hour = startHour + Math.random() * (endHour - startHour) | 0;
  date.setHours(hour);
  console.log('date',date)
  return date.toDateString();
}
  
  return (
  <div>
    <Head>
      <title>Blog</title>
      <link rel="icon" href="/favicon.ico" />
    </Head>

    <Nav />

    <div className="hero">
      <h1 id="author" className="title author">{getAuthor()}</h1>
      <p id="publishDate" className="description publishDate">{randomDate(1,20,10,11)}</p>
      <p id="modifiedDate" className="description modifiedDate">{randomDate(30,40,10,11)}</p>
    </div>

    <style jsx>{`
      .hero {
        width: 100%;
        color: #333;
      }
      .title {
        margin: 0;
        width: 100%;
        padding-top: 80px;
        line-height: 1.15;
        font-size: 48px;
      }
      .title,
      .description {
        text-align: center;
      }
      .row {
        max-width: 880px;
        margin: 80px auto 40px;
        display: flex;
        flex-direction: row;
        justify-content: space-around;
      }
      .card {
        padding: 18px 18px 24px;
        width: 220px;
        text-align: left;
        text-decoration: none;
        color: #434343;
        border: 1px solid #9b9b9b;
      }
      .card:hover {
        border-color: #067df7;
      }
      .card h3 {
        margin: 0;
        color: #067df7;
        font-size: 18px;
      }
      .card p {
        margin: 0;
        padding: 12px 0 0;
        font-size: 13px;
        color: #333;
      }
    `}</style>
  </div>
)}

export default Blog
