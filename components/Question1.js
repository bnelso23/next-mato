import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const useStyles = makeStyles(theme => ({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
  }));

const Question1 = (props) => {
    const classes = useStyles();

    const [divorceType, setDivorceType] = React.useState('');

    const handleChange = event => {
        props.handleDivorceType(event.target.value);
        setDivorceType(event.target.value);
      };

    return (
        <div>
            <FormControl className={classes.formControl}>
                <Select value={divorceType} onChange={handleChange} displayEmpty className={classes.selectEmpty}>
                    <MenuItem value="">
                        <em>Select Answer</em>
                    </MenuItem>
                    <MenuItem value={'Divorce'}>Divorce</MenuItem>
                    <MenuItem value={'Legal Seperation'}>Legal Seperation</MenuItem>
                </Select>
                {/* <FormHelperText>Without label</FormHelperText> */}
            </FormControl>
        </div>
    )
}

export default Question1;