import React, { useEffect } from "react"
import Router from "next/dist/next-server/lib/router/router";




function MyApp({ Component, pageProps }) {
    useEffect(()=>{
        console.log('I ran')

        const script = document.createElement('script');

        script.src = "/matomo.js";
        script.async = true;
      
        document.body.appendChild(script);

        Router.events.on("routeChangeStart", url => {
            if (window && window._paq) {
              window._paq.push(["setCustomUrl", url]);
              window._paq.push(["setDocumentTitle", document.title]);
              window._paq.push(["trackPageView"]);
            }
          });
      
        return () => {
          document.body.removeChild(script);
        }
    })
    return <Component {...pageProps} />
  }
  
  // Only uncomment this method if you have blocking data requirements for
  // every single page in your application. This disables the ability to
  // perform automatic static optimization, causing every page in your app to
  // be server-side rendered.
  //
  // MyApp.getInitialProps = async (appContext) => {
  //   // calls page's `getInitialProps` and fills `appProps.pageProps`
  //   const appProps = await App.getInitialProps(appContext);
  //
  //   return { ...appProps }
  // }
  
  export default MyApp