Hey Ben,

Apologies for the delay in getting this to you. It has been an absolutely crazy few weeks, but I can see a light at the end of the tunnel. As we discussed, this is an attempt to distill Chris' and my lofty ramblings into something resembling an actual plan of attack to get this thing done. Chris, if I have misstated or forgotten anything, please chime in.

As I view this project, there are several phases. Chris and I will be working on content like forms, instructions, helpful articles, marketing materials, etc. Below is what I'm hoping you can help with.

Phase 1 - Simple divorce petition (no kids)

As I see it, the first step is a proof of concept. Phase 2 will probably follow pretty close behind. What this would involve is producing a working model that can assist someone in filing for a divorce with no children. I have attached the bare minimum forms that are required. For now, this is essentially what we are going to produce. Chris and I will work to refine the forms as things progress, but these should be the forms on which the architecture is built. If we are able to preserve the formatting of the underlying forms, that would be great, but that is lower priority. The first step is to design a process that takes the client through all the questions necessary to produce the three documents attached. I have put in fields for the information that needs to be gathered.

For a first pass, all I want you to do is present one question at a time to gather the information. Here are the questions I think we need to ask:

Is this a divorce or separation? {drop down: Divorce (the actual output should say "Dissolution of Marriage"); Legal Separation }

What name shows up on your driver's license?  What's your physical address? (No P.O. Boxes)  What's your mailing address?  What's your date of birth?  What's your social security number?
What's your home phone number?  What's your cell number?  What's your work phone number?  What's your email address?   Are you in the military?

What's your soon-to-be-exe's name? What's their physical address? What's their mailing address?  What's their date of birth?  What's their social security number?
What's their home phone number?  What's their cell number?  What's their work phone number?  What's their email address?  Are they in the military?

When did you two get married?  Where did you get married?  When did you move to Colorado most recently?  When did you two stop living together?  Is either of you (or a surrogate) pregnant?  Have either of you had a restraining order of any kind issued against you in the past two years?  {If yes: Who was it?  When? What's the case number? What court issued the restraining order? Where? (county and state) What kind of restraining order was it? [drop down] What happened? [large text field]} Does either of you want your prior name restored? {If yes: What do you want your name to be?}

Each question should be presented on a separate screen or portion of the screen. Maybe one gets revealed at a time and the screen moves down to the new question? Not sure at this point, but I do think one question at a time is the way to go. For the address questions, the address (i.e., street name and number) should be a separate input from the apartment number, city, state and zip.

If possible, I'd like the system to take their street address and pick the correct court based on the county they live in. I imagine you'll need a list of which courts go with which county, and what their contact information is. Chris and I can work on that. What I am hoping you are able to figure out is how to take their address and find which county they live in, and then use that information to select the appropriate court from the list we'll give you. Is that doable?

I hope all of the above is clear enough that you can get started. I have identified several more phases below, but the above is enough to keep us busy for a while. Once you've been able to review the above and digest it, can you give us a ballpark on when a prototype might be ready for us to review? I think what you already have is a big start on the above, it's just adapting it to this particular use. Let me know if I'm totally insane.

Phase 2 - Post-petition support (separation agreement, decree, etc.)
Phase 3 - More complicated divorces
Phase 4 - Communication
Phase 5 - e-Filing
Phase 6 - Mediation support
Phase 7+ - Rule the world

On another note, I think it would be a good idea to try to get you some help on the tech side. We obviously don't have the cash to pay someone, so I think a profits interest is the way to go. Chris and I spoke yesterday and (unless you tell us not to) we are going to reach out to people we know to see if anyone is interested in assisting with the project. The way I see that being structured is as follows: Our interest in the capital would remain unchanged. We would grant a Class B profits interest to the new person. Our interests would carry a preferred rate of return (50%?), meaning that the Class B interest would not receive anything until we received all of our capital, plus the preferred return. After that threshold has been met, future profits would be split between the Class A interests (us) and the Class B interests on a 60/40 (70/30?) basis. That way, we incur no obligation to the new guy until we have been paid back, plus a return, but he gets to participate in the upside. He has an incentive to generate tons of profit without us having significant upfront cost. Let me know what you think and if you know anyone who might be interested.

Sorry for the lengthy email; that's probably enough for now.

All the best,